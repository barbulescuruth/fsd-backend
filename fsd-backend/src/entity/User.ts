import {Entity, PrimaryGeneratedColumn, Column, Generated} from "typeorm";

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    email: string;

    @Column()
    userName: string;

    @Column()
    password: string;

    @Column({nullable: true})
    token: string;

}
