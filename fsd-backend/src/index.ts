import "reflect-metadata";
import {createConnection, getRepository} from "typeorm";
import * as express from "express";
import * as upload from "express-fileupload";
import * as cors from "cors";
import * as bodyParser from "body-parser";
import {Request, Response} from "express";
import {Routes} from "./routes";
import { User } from "./entity/User";
import * as jwt from "jsonwebtoken";
import run from "./tensorflow/script";
import { MnistData } from "./tensorflow/data";
import { json } from "body-parser";



function toArrayBuffer(buf){
  var ab = new ArrayBuffer(buf.length);
  var view = new Uint8Array(ab);
  for (var i = 0; i < buf.length; ++i) {
      view[i] = buf[i];
  }
  return ab;

}

createConnection().then(async () => {

    // create express app
    const app = express();
    app.use(bodyParser.json());

    // register express routes from defined application routes
    Routes.forEach(route => {
        (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
            const result = (new (route.controller as any))[route.action](req, res, next);
            if (result instanceof Promise) {
                result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);

            } else if (result !== null && result !== undefined) {
                res.json(result);
            }
        });
    });

    app.use(upload());
    app.use(cors());

    app.post("/evaluate", async function (req, res) {
      try {
        const file = req.files.image as upload.UploadedFile;
        const bodyData = req.body;

        const reqDataBuffer = toArrayBuffer(file.data);
        console.log('here' + reqDataBuffer);

        const data = new MnistData();
        const pixels = data.load(reqDataBuffer);
        const prediction = run(data, pixels);
        
        console.log(prediction);
        res.status(200).send(JSON.stringify(prediction));
      } catch (error) {
          res.send("ERROR");
      }
    });

    // importing user context

    let userRepository = getRepository(User);
    const bcrypt = require("bcrypt")
    app.post("/register", async (req, res) =>{
        try {
            // Get user input
            const { email, userName, password } = req.body;
        
            // Validate user input
            if (!(email && userName && password )) {
              res.status(400).send("All input is required");
            }
        
            // check if user already exist
            // Validate if user exist in our database
            const oldUser = await userRepository.findOne({ email });
        
            if (oldUser) {
              return res.status(409).send("User Already Exist.Login!");
            }
        
            //Encrypt user password
           
           const encryptedPassword = await bcrypt.hash(password, 10);
        
            // Create user in our database
            const user = await userRepository.create({
              email: email.toLowerCase(), // sanitize: convert email to lowercase
              userName,
              password: encryptedPassword,
            });
            
            userRepository.save(user);
        
            // return new user
            res.status(201).json(user);
          } catch (err) {
            console.log(err);
          }
          // Our register logic ends here
        });
    app.post("/login", async(req, res) =>{
      try{
        const {email, password} = req.body;

        if(!(email && password)){
          res.status(400).send("All input is required");
        }

        const user  = await userRepository.findOne({email});

        if (user && (await bcrypt.compare(password, user.password))) {
          // Create token
          const token = jwt.sign(
            { user_id: user.id, email },
            process.env.TOKEN_KEY,
            {
              expiresIn: "2h",
            }
          );
    
          // save user token
          user.token = token;
    
          // user
          res.status(200).json(user);
        }
        res.status(400).send("Invalid Credentials");
      } catch(err){
        console.log(err);
      }
    });

    const auth = require("./middleware/authentication");
    app.post("/welcome", auth, (req, res) => {
      res.status(200).send("Welcome 🙌");
    });

    // setup express app here
    // ...

    // start express server
    app.listen(process.env.PORT);

    console.log("Express server has started on port 3000. Open http://localhost:3000/users to see results");

}).catch(error => console.log(error));


