const {Datastore} = require('@google-cloud/datastore');
const datastore = new Datastore({
	projectId: 'gcp-fsd-project',
	keyFilename: 'datastore-credential.json'
});
const kindName = 'schedule';
const cors = require('cors')


//https://us-central1-gcp-fsd-project.cloudfunctions.net/saveSchedule
exports.saveSchedule = (req, res) => { 
	
	res.set('Access-Control-Allow-Origin', '*');

	if(req.method == 'OPTIONS'){
		res.set('Access-Control-Allow-Methods', 'POST');
		res.set('Access-Control-Allow-Headers', 'Content-Type');
		return res.status(200).send("");
	}
	
	let startDate = req.body.startDate;
	let endDate = req.body.endDate;

	datastore
		.save({
			key: datastore.key(kindName),
			data: {
				startDate: startDate.toString(),
				endDate: endDate.toString(),
			}
		})
		.then(() => {
			res.status(200).send({"startDate" : startDate, "endDate" :endDate});
		})
		.catch(err => {
			console.error('ERROR:', err);
			res.status(500).send(err);
			return;
		});

		// if(req.method == 'OPTIONS'){
		// 	res.set('Access-Control-Allow-Methods', 'POST');
		// 	res.set('Access-Control-Allow-Headers', 'Content-Type');
		// } else {
		// 	res.status(200).send({"startDate" : startDate, "endDate" :endDate});
		// }
	
};

//https://us-central1-gcp-fsd-project.cloudfunctions.net/getSchedules
exports.getSchedules = (req, res) => {

	res.set('Access-Control-Allow-Origin', '*');
	const query = datastore.createQuery('schedule');
	datastore.runQuery(query)
		.then(results => {
			const schedules = results[0];
			let finalSchedules = [];
			schedules.forEach(schedule => {
				finalSchedules.push({"startDate" : schedule.startDate, "endDate" : schedule.endDate});
			})

			if(req.method == 'OPTIONS'){
				res.set('Access-Control-Allow-Methods', 'GET');
				res.set('Access-Control-Allow-Headers', 'Content-Type');
			}else {
				res.status(200).send(JSON.stringify(finalSchedules));
			}

		}).catch(err => {
		    console.error('ERROR:', err);
		    res.status(500).send(err);
		    return;
		});
	
};